var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var neat = require('node-neat');

var node_modules = './node_modules/';
var js_libs = [
    node_modules+'parsleyjs/dist/parsley.min.js',
    node_modules+'urijs/src/URI.min.js',
    node_modules+'lity/dist/lity.min.js',
    node_modules+'js-cookie/src/js.cookie.js'
];

elixir.config.assetsPath = 'src';
elixir.config.publicPath = 'dist';
elixir.config.css.sass.folder = 'scss';

//Build out the paths to the sass libraries
var sassIncludePaths = [
    __dirname+'/node_modules/normalize-libsass'
];
sassIncludePaths = sassIncludePaths.concat(require('node-neat').includePaths);

elixir(function(mix) {
    mix.sass('agegate.scss', 'dist/css/', {includePaths: sassIncludePaths});
    mix.less('./node_modules/lity/src/lity.less', 'dist/css/lity.css');

    mix.copy(node_modules+'jquery/dist/jquery.min.js', 'dist/js/');
    mix.scripts(js_libs, 'dist/js/vendor.js');
    mix.scripts('agegate.js', 'dist/js/');
});