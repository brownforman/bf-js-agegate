## BF Javascript Agegate Example
This is an example agegate that contains all of the logic needed to meet our compliance standards.
### Functionality Overview
* Display an agegate form over top of content inside of a lightbox if the user has not age-affirmed and gotten an `lda` cookie set
* Take a year, month, day and send them to the Brown-Forman web service to validate if the user is of legal drinking age

### Basic Installation
1. Include the stylesheets from `dist/css`
2. Include jQuery, `dist/js/vendor.js`, and `dist/js/agegate.js`
3. Include the HTML found in the `agegate` div of `example.html` on every one of your pages (probably best to use a template of some sort)
4. (Optional) Add data-default-iso="[2 letter country code]" to the country element in order to default the country. Defaults to us if this attribute is not specified. Ex.: <select name="country" data-default-iso="gb"></select>

### Advanced
If you customize the Javascript or the styles found in `/src/` you will need to use gulp to compile and output your custom build to `/dist/`.

If you have NPM/Nodejs installed you can simply run these commands inside of your working copy

1. `npm install`
2. `gulp`

The gulp configuration of this project will also allow you to use `gulp watch` to watch the source files for changes and automatically trigger builds.  You may also want to use `gulp --production` to trigger a minified production ready build of the JS/CSS files.