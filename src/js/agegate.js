function setup_agegate(){
	$.ajax({
		url: "http://bf-webservice.herokuapp.com/api/countries",
		dataType: "json",
		type : "GET",
		success : function(r) {
			var html = "";
			var country = $('select[name=country]');
			var default_iso = (country.attr('data-default-iso') !== 'undefined' && country.attr('data-default-iso') !== '') ? country.attr('data-default-iso') : "us";
			$.each(r, function(key, value){
				if(value.iso == default_iso){
					html += "<option value='"+value.iso+"' selected='selected'>"+value.name+"</option>\n";
				} else{
					html += "<option value='"+value.iso+"'>"+value.name+"</option>\n";
				}
			});
			country.html(html);
		}
	});

	var agegatelightbox = lity({
		'esc': false,
		'template': '<div class="lity" tabindex="-1"><div class="lity-wrap"><div class="lity-loader">Loading...</div><div class="lity-container"><div class="lity-content"></div></div></div></div>'
	});

	agegatelightbox($('div#agegate'));

	$('form[name=agegate]').parsley();

	$('form[name=agegate]').on('submit', function(e){
		e.preventDefault();

		if($(this).parsley().isValid()){
			$.ajax({
				url: "http://bf-webservice.herokuapp.com/api/validatelda",
				method: "POST",
				data: {
				    month: $('input[name=month]').val(),
				    day: $('input[name=day]').val(),
				    year: $('input[name=year]').val(),
				    country: $('select[name=country]').val(),
				    category: $('input[name=category]').val()
				},
				success: function(data){
					if(data){
						if( $('input[name=remember]').is(':checked') ){
							Cookies.set('lda', 1, {expires: 365});
						} else {
							var date = new Date();
							var minutes = 120;
							date.setTime(date.getTime() + (minutes*60*1000));
							Cookies.set('lda', 1, {expires: date});
						}

						if (!window.location.hash){
							window.scrollTo(0, 0);
						}

						agegatelightbox.close();
					}
					else{
						var date = new Date();
						var minutes = 60;
						date.setTime(date.getTime() + (minutes*60*1000));
						Cookies.set('lda', 0, {expires: date});
						redirect_invalidlda();
					}
				},
				statusCode:{
					400: function(data){
						$('div.agegate-errors').css('display', 'block');
						$('div.agegate-errors').html('<p>' + data.responseText + '</p>');
					}
				}
			});
		}

	});
}

function redirect_invalidlda(){
	$('div#agegate').html("<h3>Sorry you are not of legal drinking age.</h3><h3>Redirecting in <span id='countdown'>10</span> seconds...");
	var count = 10;
	var countdown = setInterval(function(){
		$('span#countdown').html(count);
		if(count === 0){
			clearInterval(countdown);
			window.location.href = 'http://responsibility.org/';
		}
		count--;
	}, 1000);
}

$(document).ready(function(){
	if( typeof Cookies.get('lda') != 'undefined' ){
		if(Cookies.get('lda') == 0){
			setup_agegate();
			redirect_invalidlda();
		}
	}
	else{
		//Check for trust=yes
		var url = new URI(window.location.href);
		if(url.search(true)['trust'] == 'yes'){
			var date = new Date();
			var minutes = 120;
			date.setTime(date.getTime() + (minutes*60*1000));
			Cookies.set('lda', 1, {expires: date});
		} else {
			setup_agegate();
		}
	}
});
